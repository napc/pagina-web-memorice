<?php
	session_start();/* Abre una sesion preexistente */
	if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {}
	else {
		header('Location: home.php');
		exit;
	}
		/* Sentencia para que solo los usuarios registrados puedan ver el contenido de la pagina*/
	include "permissions.php";
	if ($perm > 0) {}
	else {
		echo "Tu cuenta ha sido deshabilitada, por favor contacta a un administrador. <br>"."Redireccionando...";
		header('refresh:2; url=/whoweare.php');
		session_destroy();
		echo '<img src="/images/loading.gif" height="70px" width="70px"></img>';
		exit;
	}

	/* Esta sentencia hace que los usuarios deshabilitados no puedan entrar a esta pagina */
		if ($perm > 1) {}
	else {
		echo "No tienes permisos para estar en esta pagina."."Redireccionando...";
		header('refresh:2; url=/game.php');
		echo '<img src="/images/loading.gif" height="70px" width="70px"></img>';
		exit;
	}
	/* Esta sentencia hace que los usuarios anonimos no puedan entrar a la pagina */
?>
<?php
	$servername = "localhost";
	$username = "root";
	$password = "";
	$dbname = "usuarios";
	$tbl_name = "login";
	// Crea conexion
	$conn = mysqli_connect($servername, $username, $password, $dbname);
	// Checkea coneccion
	if (!$conn) {
		die("Connection failed: " . mysqli_connect_error());
	}
	mysqli_set_charset($conn,"utf8");
	$oldpass = $_POST['password1']; //guarda la contraseña anterior 
	$newpass = $_POST['password2']; //guarda la contraseña nueva
	$oldhash = password_hash($oldpass, PASSWORD_BCRYPT); //encripta la contraseña anterior
	$newhash = password_hash($newpass, PASSWORD_BCRYPT); //encripta la contraseña nueva
	$newpass = "UPDATE $tbl_name SET password = '$newhash' WHERE username = '$_SESSION[username]'"; 
	$sql = "SELECT * FROM $tbl_name WHERE username = '$_SESSION[username]'";
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {     
	}
	$row = $result->fetch_array(MYSQLI_ASSOC);
	if (password_verify($oldpass, $row['password'])){ //verifica la contraseña
		if ($_POST['password2'] == $_POST['password3']){ //verifica que las contraseñas coincidan
			if ($conn->query($newpass) === TRUE) {
				echo "<br />" . "<h2>" . "Contraseña cambiada!" . "</h2><br>"."Redireccionando...";
				header('refresh:2; url=../panel-control.php');
				echo '<img src="/images/loading.gif" height="70px" width="70px"></img>';
			}
			else {
				echo "Error cambiar Contraseña" . $conn->error; 
			}
		}
		else {
			echo "Las contraseñas no coinciden. Redireccionando...";
			echo '<img src="/images/loading.gif" height="70px" width="70px"></img>';
			header('refresh:2; url=../panel-control.php');
		}
	}
	else{
		echo "Contraseña actual incorrecta". "</h2><br>"."Redireccionando...";
		echo '<img src="/images/loading.gif" height="70px" width="70px"></img>';
		header('refresh:2; url=editpass.php');
	}
	mysqli_close($conn);
?>