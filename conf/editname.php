<?php
	session_start();/* Abre una sesion preexistente */
	if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {}
	else {
		header('Location: home.php');
		exit;
	}
		/* Sentencia para que solo los usuarios registrados puedan ver el contenido de la pagina*/
	include "permissions.php";
	if ($perm > 0) {}
	else {
		echo "Tu cuenta ha sido deshabilitada, por favor contacta a un administrador. <br>"."Redireccionando...";
		header('refresh:2; url=/whoweare.php');
		session_destroy();
		echo '<img src="/images/loading.gif" height="70px" width="70px"></img>';
		exit;
	}

	/* Esta sentencia hace que los usuarios deshabilitados no puedan entrar a esta pagina */
		if ($perm > 1) {}
	else {
		echo "No tienes permisos para estar en esta pagina."."Redireccionando...";
		header('refresh:2; url=/game.php');
		echo '<img src="/images/loading.gif" height="70px" width="70px"></img>';
		exit;
	}
	/* Esta sentencia hace que los usuarios anonimos no puedan entrar a la pagina */
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>
		Configuracion - M3M0R1C3
		</title>
		<link href="/css/estilos.css" media="screen" rel="stylesheet" type="text/css"/>
		<meta content="width=device-width, initial-scale=1" name="viewport">
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js">
		</script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js">
		</script>
		<!-- Links para implementar bootstrap y jquery -->
	</link>
	</meta>
</head>
<body>
	<header>
		<!-- Cabecera del sitio, donde va la barra de navegación -->
		<?php include "../navbar/navbar.php"; ?>
		<!-- Barra de navegación -->
	</header>
	<div class="container">
		<!-- Este div hace que los elementos queden centrados en la pagina -->
		<section class="main row">
			<article class=" col-md-6">
				<!-- El articulo ocupa las columnas señaladas en la clase -->
				<h1>
				Editar Nombre Completo
				</h1>
				<form action="chname.php" method="post">
					<p>
						Nuevo nombre
					</p>
					<input autofocus="" class="form-control" name="name" type="text"/>
					<p>
						y/o nuevos apellidos
					</p>
					<input class="form-control" name="ap" type="text"/>
					<input class="btn btn-primary" type="submit" value="Enviar"/>
				</form>
			</article>
		</section>
	</div>
</body>
</html>
</br>