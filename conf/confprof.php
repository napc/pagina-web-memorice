<?php
	session_start();/* Abre una sesion preexistente */
	if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {}
	else {
		header('Location: home.php');
		exit;
	}
		/* Sentencia para que solo los usuarios registrados puedan ver el contenido de la pagina*/
	include "permissions.php";
	if ($perm > 0) {}
	else {
		echo "Tu cuenta ha sido deshabilitada, por favor contacta a un administrador. <br>"."Redireccionando...";
		header('refresh:2; url=/whoweare.php');
		session_destroy();
		echo '<img src="/images/loading.gif" height="70px" width="70px"></img>';
		exit;
	}

	/* Esta sentencia hace que los usuarios deshabilitados no puedan entrar a esta pagina */
		if ($perm > 1) {}
	else {
		echo "No tienes permisos para estar en esta pagina."."Redireccionando...";
		header('refresh:2; url=/game.php');
		echo '<img src="/images/loading.gif" height="70px" width="70px"></img>';
		exit;
	}
	/* Esta sentencia hace que los usuarios anonimos no puedan entrar a la pagina */
?>

<?php
	$servername = "localhost";
	$username = "root";
	$password = "";
	$dbname = "usuarios";
	$tbl_name = "login";
	// Crea conexion
	$conn = mysqli_connect($servername, $username, $password, $dbname);
	// Checkea coneccion
	if (!$conn) {
		die("Connection failed: " . mysqli_connect_error());
	}

	$target_path = "../images/profile/"; //define la ruta a guardar de la imagen
	$target_path = $target_path . basename( $_FILES['uploadedfile']['name']);
	$img =  basename( $_FILES['uploadedfile']['name']);//devuelve la ruta del archivo

	if (!($_FILES['uploadedfile']['type'] =="image/jpeg" OR $_FILES['uploadedfile']['type'] =="image/gif")) //comprueba que la imagen sea formato jpg o png
	{
		echo "El archivo no corresponde a una imagen jpg o gif ";
		echo "<br><a href='profpic.php'>Volver a Intentarlo</a>";
	}
	else{

		$sql = "UPDATE $tbl_name SET profpic = '$img' WHERE username = '$_SESSION[username]'";
		//guarda la ruta del archivo en un sql
		if (mysqli_query($conn, $sql)) {

			if(move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $target_path)) {
			//comprueba que la imagen se haya subido 
				echo "La imagen se ha subido correctamente"."<br>"."Redireccionando...";
				echo '<img src="/images/loading.gif" height="70px" width="70px"></img>';
				header('refresh:2; url=../panel-control.php');
			}
			else{
				echo "Ha ocurrido un error!";
				echo "<br><a href='profpic.php'>Volver a Intentarlo</a>";
			}
		}

		else {
			echo mysqli_error($conn);
		}
	}
	mysqli_close($conn);

 ?>