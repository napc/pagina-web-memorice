<?php
	session_start(); 
	if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
		header('Location: index.php');
	}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Ingresar - M3M0R1C3</title>
		<meta charset = "utf-8">
		<link rel="stylesheet" type="text/css" href="/css/estilos.css" media="screen" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<!-- Links para implementar bootstrap y jquery -->
	</head>
	<body>
		<header><!-- Cabecera del sitio, donde va la barra de navegación -->
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand" href="/home.php">M3M0R1C3</a>
				</div>
				<ul class="nav navbar-nav">
					<li><a href="/login.php">Login</a></li>
					<li><a href="/reg.php">Registrarme</a></li>
					<li><a href="/whoweare.php">Acerca de</a></li>
				</ul>
			</div>
			<!-- Barra de navegacion -->
		</header>
		<div class="container"><!-- Este div hace que los elementos queden centrados en la pagina -->
		<section class="main row"><!-- Agrupa elementos y los separa en columnas y filas -->
		
		<article class=" col-md-6"><!-- El articulo ocupa las columnas señaladas en la clase -->
			<form action="/conf/sendmail.php" method="post" >
				<p>Ingrese su Correo</p>
				<input class="form-control" name="email" type="email" required  autofocus />
				<input class="btn btn-primary" type="submit" value="Enviar"/>
			</form>
			
		</article>
	</section>
</div>
</body>
</html>