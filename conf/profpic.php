<?php
	session_start();/* Abre una sesion preexistente */
	if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {}
	else {
		header('Location: home.php');
		exit;
	}
		/* Sentencia para que solo los usuarios registrados puedan ver el contenido de la pagina*/
	include "permissions.php";
	if ($perm > 0) {}
	else {
		echo "Tu cuenta ha sido deshabilitada, por favor contacta a un administrador. <br>"."Redireccionando...";
		header('refresh:2; url=/whoweare.php');
		session_destroy();
		echo '<img src="/images/loading.gif" height="70px" width="70px"></img>';
		exit;
	}

	/* Esta sentencia hace que los usuarios deshabilitados no puedan entrar a esta pagina */
		if ($perm > 1) {}
	else {
		echo "No tienes permisos para estar en esta pagina."."Redireccionando...";
		header('refresh:2; url=/game.php');
		echo '<img src="/images/loading.gif" height="70px" width="70px"></img>';
		exit;
	}
	/* Esta sentencia hace que los usuarios anonimos no puedan entrar a la pagina */
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>
		Configuracion - M3M0R1C3
		</title>
		<meta charset="utf-8">
		<link href="/css/estilos.css" media="screen" rel="stylesheet" type="text/css"/>
		<meta content="width=device-width, initial-scale=1" name="viewport">
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js">
		</script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js">
		</script>
		<!-- Links para implementar bootstrap y jquery -->
	</link>
	</meta>
	</meta>
</head>
<body>
	<header>
		<!-- Cabecera del sitio, donde va la barra de navegación -->
		<?php include "../navbar/navbar.php";?>
	</header>
	<div class="container">
		<!-- Este div hace que los elementos queden centrados en la pagina -->
		<section class="main row">
			<!-- Agrupa elementos y los separa en columnas y filas -->
			<article class=" col-md-6">
				<!-- El articulo ocupa las columnas señaladas en la clase -->
				<h1>
				Añadir/Editar Foto de Perfil
				</h1>
				<p>
					Para cambiar la foto de perfil, por favor, suba una desde su equipo.
				</p>
				<form action="confprof.php" enctype="multipart/form-data" method="post">
					<input class="btn btn-primary" name="uploadedfile" required="" type="file"/><!-- Aca se sube un archivo al servidor -->
					<br>
					<br>
					<input class="btn btn-primary" type="submit" value="Cambiar"/>
					</br>
					</br>
				</form>
				<a href="delprofpic.php">
					<button "="" class="btn btn-primary"><!-- Deja la imagen por defecto -->
					Eliminar foto de perfil
					</button>
				</a>
			</article>
		</section>
	</div>
</body>
</html>
</br>