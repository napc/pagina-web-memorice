<?php
	session_start();
	if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
		header('Location: index.php');
		}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Registrarse - M3M0R1C3</title>
	<meta charset = "utf-8">
		<link rel="stylesheet" type="text/css" href="/css/estilos.css" media="screen" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<!-- Links para implementar bootstrap y jquery -->
</head>
<body>
		<header><!-- Cabecera del sitio, donde va la barra de navegación -->
			<nav class="navbar navbar-default">
				<div class="container-fluid">
					<div class="navbar-header">
						<a class="navbar-brand" href="/home.php">M3M0R1C3</a>
					</div>
					<ul class="nav navbar-nav">
						<li><a href="/login.php">Login</a></li>
						<li class="active"><a href="/reg.php">Registrarme</a></li>
						<li><a href="/whoweare.php">Acerca de</a></li>
					</ul>
				</div>
			</nav>
			<!-- Barra de navegacion -->
		</header>

		<div class="container"><!-- Este div hace que los elementos queden centrados en la pagina -->
		<section class="main row">
		<article class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
			<form action="registrar-usuario.php" method="post"> <!-- Envia los datos al archivo de conexion -->
				<font size="4" ><h3>Crear una cuenta</h3></font>
				<p>Por favor ingrese sus datos:</p>
				<input class="form-control" placeholder="Nombre" name="name" type="text" id="username" required autofocus>
				<br>
				<input class="form-control" placeholder="Apellidos" name="ap" type="text" id="username" required >
				<br>
				<input class="form-control" placeholder="Correo Electronico" name="email" type="email" id="username" required>
				<br>
				<input class="form-control" placeholder="Usuario" name="username" type="text" id="username" required>
				<br>
				<input class="form-control" placeholder="Contraseña" name="password" type="password" id="password" required>
				<br>
				<input class="btn btn-primary" type="reset" name="clear" value="Borrar">
				<input class="btn btn-primary" type="submit" name="submit" value="Registrarme">
				

			</form>
		</article>
		</section>
		</div>
</body>
</html>