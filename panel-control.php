<?php
	session_start(); /* Abre una sesion preexistente */
	if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {}
	else {
		header('Location: home.php');
		exit;
	}
		/* Sentencia para que solo los usuarios registrados puedan ver el contenido de la pagina*/
	include "/conf/permissions.php";
	if ($perm > 0) {}
	else {
		echo "Tu cuenta ha sido deshabilitada, por favor contacta a un administrador. <br>"."Redireccionando...";
		header('refresh:2; url=/login.php');
		session_destroy();
		echo '<img src="/images/loading.gif" height="70px" width="70px"></img>';
		exit;
	}

	/* Esta sentencia hace que los usuarios deshabilitados no puedan entrar a esta pagina */
		if ($perm > 1) {}
	else {
		echo "No tienes permisos para estar en esta pagina."."Redireccionando...";
		header('refresh:2; url=/game.php');
		echo '<img src="/images/loading.gif" height="70px" width="70px"></img>';
		exit;
	}
	/* Esta sentencia hace que los usuarios anonimos no puedan entrar a la pagina */
	include "/conf/conn.php";
	$sname = "SELECT name FROM $tbl_name WHERE username = '$_SESSION[username]'";
	$sap = "SELECT ap FROM $tbl_name WHERE username = '$_SESSION[username]'";
	$smail = "SELECT email FROM $tbl_name WHERE username = '$_SESSION[username]'";
	/* Aca se consigue con la base de datos los datos para luego ser implementados*/
	$resultname = $conn->query($sname);
	$resultap = $conn->query($sap);
	$resultmail = $conn->query($smail);
	/* Luego se hacen las consultas*/
	if (mysqli_query($conn, $sname)) {
		while ($row = $resultname->fetch_assoc()) {
			$name = $row['name'];
		}
	}
	if (mysqli_query($conn, $sname)) {
		while ($row = $resultap->fetch_assoc()) {
			$ap = $row['ap'];
		}
	}
	if (mysqli_query($conn, $smail)) {
		while ($row = $resultmail->fetch_assoc()) {
			$email = $row['email'];
		}
	}
	else {
echo mysqli_error($conn);
	}
	/* Se pasan los datos a variables*/
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Configuracion - M3M0R1C3</title>
		<meta charset = "utf-8">
		<link rel="stylesheet" type="text/css" href="/css/estilos.css" media="screen" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<!-- Links para implementar bootstrap y jquery -->
	</head>
	<body>
		<header> <!-- Cabecera del sitio, donde va la barra de navegación -->
		<?php include "navbar/navbar.php"; ?>
		
	</header>
	
	<div class="container">
		<!-- Este div hace que los elementos queden centrados en la pagina -->
		<section class="main row"> <!-- Agrupa elementos y los separa en columnas y filas -->
		<article class="col-xs-12 col-sm-8 col-md-8 col-lg-9"><!-- El articulo ocupa las columnas señaladas en la clase -->
		<h1>Panel de Control</h1>
		<p>Aqui puedes configurar los datos de tu cuenta</p>
		<a href="conf/profpic.php"><button class="btn btn-primary" >Cambiar foto de perfil</button></a>
		<p><?php echo "<strong>Nombre: </strong>".$name." ",$ap; ?></p>
		<a href="conf/editname.php"><button class="btn btn-primary" >Cambiar</button></a>
		<p><?php echo "<strong>Nombre de Usuario: </strong>".$_SESSION['username']; ?></p>
		<a href="conf/edituser.php"><button class="btn btn-primary" >Cambiar</button></a>
		<p> <?php echo "<strong>E-mail: </strong>".$email; ?></p>
		<a href="conf/editemail.php"><button class="btn btn-primary" >Cambiar</button></a>
	</article>
	<aside class="col-xs-12 col-sm-4 col-md-4 col-lg-3">
		<h2>Seguridad</h2>
		<br>
		<a href="conf/editpass.php"><button class="btn btn-primary" >Cambiar Contraseña</button></a>
		<?php
			include "/conf/conn.php"; /* Se inserta el archivo de conexion de mysql a a la pagina */
			$adm = "SELECT * FROM $tbl_name WHERE username='$_SESSION[username]' and estado='3'";
			$result = $conn->query($adm);
			$count = mysqli_num_rows($result);
			if ($count == 1) {
			/* Devuelve el permiso de la cuenta actual para mostrar el menu de admin*/
		?>
		<a href="admin.php"><button class="btn btn-primary" >Menu administrador</button></a>
		<?php }
			else{
		
			}
			mysqli_close($conn);
		?>
	</aside>
</section>
</div>
</body>
</html>