<?php
	session_start();
	if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
		header('Location: index.php');
		}
?> <!-- Si la sesion esta iniciada redirige al index del usuario-->
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Quienes somos - M3M0R1C3</title>
		<meta charset = "utf-8">
		<link rel="stylesheet" type="text/css" href="/css/estilos.css" media="screen" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<!-- Links para implementar bootstrap y jquery -->
		<body>
			
			<header><!-- Cabecera del sitio, donde va la barra de navegación -->
			<nav class="navbar navbar-default">
				<div class="container-fluid">
					<div class="navbar-header">
						<a class="navbar-brand" href="/home.php">M3M0R1C3</a>
					</div>
					<ul class="nav navbar-nav">
						<li><a href="/login.php">Login</a></li>
						<li><a href="/reg.php">Registrarme</a></li>
						<li class="active"><a href="/whoweare.php">Acerca de</a></li>
					</ul>
				</div>
			</nav>
		</header>
	</div>
	<div class="container">
		<!-- Este div hace que los elementos queden centrados en la pagina -->
		<section class="main row">
			<article class="col-xs-12 col-sm-8 col-md-8 col-lg-9"><!-- El articulo ocupa las columnas señaladas en la clase -->
			<font size="4" ><h3>Quienes somos</h3></font>
			<p>Somos alumnos del Liceo Politénico Andes de la fundación DUOC. 
			<br>Nuestros nombres son: 
			<ul>Nicolas Peña 18 Años</ul>
			<ul>Danitza Zuñiga 18 Años</ul>
			<ul>Jesus Quiroz 17 Años</ul>
			<ul>Javiera Montecino 17 Años</ul>
		</p>
			<a href="doc.pdf" target=”_blank”><button class="btn btn-primary">Documentación</button>
				<a href="https://bitbucket.org/napc/pagina-web-memorice/src" target=”_blank”><button class="btn btn-primary"">Ver GIT de la pagina</button></a>
				<div class="form-group">
					<!-- Formulario para enviar una sugerencia -->
					<h1>Escribanos un mensaje</h1>
					<form action="/conf/savesug.php" method="post" >
						<input class="form-control" placeholder="Nombre" name="nombre" type="text">
						<textarea placeholder="Escriba aqui un mensaje" class="form-control" rows="5" id="comment" name="sug"required></textarea>
						<input type="submit" class="btn btn-primary" name="Submit" Value="Enviar">
					</form>
				</div>
			</article>
		</section>
	</div>
</body>
</html>