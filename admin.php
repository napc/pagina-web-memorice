<?php
	session_start();/* Abre una sesion preexistente */
	if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {}
	else {
		header('Location: home.php');
		exit;
	}
		/* Sentencia para que solo los usuarios registrados puedan ver el contenido de la pagina*/
	include "/conf/permissions.php";
	if ($perm > 0) {}
	else {
		echo "Tu cuenta ha sido deshabilitada, por favor contacta a un administrador. <br>"."Redireccionando...";
		header('refresh:2; url=/login.php');
		session_destroy();
		echo '<img src="/images/loading.gif" height="70px" width="70px"></img>';
		exit;
	}
	/* Esta sentencia hace que los usuarios deshabilitados no puedan entrar a esta pagina */
	


	include "/conf/permissions.php";
	if ($perm > 2) {}
	else {
		echo "No tienes permiso para estar en esta pagina.<br>"."Redireccionando...";
		header('refresh:2; url=../panel-control.php');
		echo '<img src="/images/loading.gif" height="70px" width="70px"></img>';
		exit;
	}
	/* Sentencia para que solo el admin pueda acceder a esta pagina*/
	include "/conf/conn.php";
	mysqli_close($conn);
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Configuracion - M3M0R1C3</title>
		<meta charset = "utf-8">
		<link rel="stylesheet" type="text/css" href="/css/estilos.css" media="screen" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<!-- Links para implementar bootstrap y jquery -->
	</head>
	<body>
		<header> <!-- Cabecera del sitio, donde va la barra de navegación -->
		<?php include "navbar/navbar.php"; ?>
		<!-- Barra de navegación -->
	</header>
	<div class="container">
		<section class="main row"> <!-- Agrupa elementos y los separa en columnas y filas -->
		<article class=" col-md-6">
			<h1>Panel de Control</h1>
			<p>Aqui puedes configurar Cuentas</p>
			<?php
					include "/conf/conn.php"; /* Se inserta el archivo de conexion de mysql a a la pagina */
					$adm = "SELECT * FROM $tbl_name WHERE username='$_SESSION[username]' and estado='3'";
					$result = $conn->query($adm);
					$count = mysqli_num_rows($result);
					if ($count == 1) {
					/* Devuelve el permiso de la cuenta actual para mostrar el cambio de permisos*/
			?>
			<p><strong> Cambiar estado de cuenta: </strong></p>
			<form action="conf/editstate.php" method="post"><!-- Formulario para cambiar persmiso a un usuario -->
			<?php
				echo '<select class="form-control" name="cambiar" required>';
								echo '<option value=""> </option>';
								$sql='select username from login '; // Consulta para recolectar a los usuario
								foreach ($conn->query($sql) as $row) {
								echo '<option value='.$row['username'].'>'.$row['username'].'</option>'; // Formato para añadir opciones
								}
				echo '</select></br>';
			?>
			<input type="radio" name="estado" value="0" required> Deshabilitar<br>
			<input type="radio" name="estado" value="2"> Habilitar<br>
			<input class="btn btn-primary"type="submit" name="submit" value="Cambiar">
		</form>
		<?php } //else para ocultar el menu a los usuarios con mas derechos
			else{
		
			}
			mysqli_close($conn);
		?>
		<p><strong> Eliminar cuenta: </strong></p>
		<form action="conf/deletethis.php" method="post"> <!-- Formulario para eleminar una cuenta -->
		<?php
			include "/conf/conn.php"; /* Se inserta el archivo de conexion de mysql a a la pagina */
			echo '<select class="form-control" name="eliminar" placeholder="Cuenta a eliminar" required>';
							echo '<option value=""> </option>';
							$sql='select username from login '; // Consulta para recolectar a los usuarios
							foreach ($conn->query($sql) as $row) {
							echo '<option value='.$row['username'].'>'.$row['username'].'</option>'; // Formato para añadir opciones
						}
		echo '</select></br>';
		?>
		<input class="btn btn-primary" type="submit" name="submit" value="Eliminar">
	</form>
</article>
</section>
</div>
</body>
</html>