<?php
	$host_db = "localhost";
	$user_db = "root";
	$pass_db = "";
	$db_name = "usuarios";
	$tbl_name = "login";
	$form_pass = $_POST['password']; //deja la contraseña en una variable
	$hash = password_hash($form_pass, PASSWORD_BCRYPT); //encripta la contraseña
	//crea la conexion
	$conexion = new mysqli($host_db, $user_db, $pass_db, $db_name);

	if ($conexion->connect_error) {
		die("La conexion falló: " . $conexion->connect_error);
	}
	//comprueba la conexion
	mysqli_set_charset($conexion,"utf8");
	$buscarUsuario = "SELECT * FROM $tbl_name WHERE username = '$_POST[username]' "; //busca el usuario ingresado para que no se repita.
	$result = $conexion->query($buscarUsuario);
	$count = mysqli_num_rows($result);
	if ($count == 1){ //comprueba que no haya un usario con el mismo nombre
		echo "<br />". "El Nombre de Usuario utilizado." . "<br />";
		echo "<a href='reg.php'>Por favor escoga otro Nombre</a>";
	}
	else{
		$query = "INSERT INTO login (name, ap, email, username, password, estado) VALUES ('$_POST[name]','$_POST[ap]','$_POST[email]','$_POST[username]','$hash','2')"; //ingresa los datos del usuario a la base de datos
		if ($conexion->query($query) === TRUE) { //comprueba que la consulta se realizo correctamente
			echo "<br />" . "<h2>" . "Usuario Creado Exitosamente!" . "</h2>";
			echo "<h4>" . "Bienvenido: " . $_POST['username'] . "</h4>" . "Redireccionando, espere...";
			echo '<img src="/images/loading.gif" height="70px" width="70px"></img>';
			header('refresh:2; url=login.php');
		}
		else {
			echo "Error al crear el usuario." . $query . "<br>" . $conexion->error; 
		}
	}
	mysqli_close($conexion);
?>