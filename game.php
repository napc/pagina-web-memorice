<?php
session_start();
if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {} 
else {
   header('Location: home.php');
exit;
}
include "/conf/permissions.php";
    if ($perm > 0) {}
    else {
        echo "Tu cuenta ha sido deshabilitada, por favor contacta a un administrador. <br>"."Redireccionando...";
        header('refresh:2; url=/login.php');
        session_destroy();
        echo '<img src="/images/loading.gif" height="70px" width="70px"></img>';
        exit;
    }
    /* Esta sentencia hace que los usuarios deshabilitados no puedan entrar a esta pagina */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Ingresar - M3M0R1C3</title>
    <meta charset = "utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/css/game.css">
    <link rel="stylesheet" href="/css/estilos.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    
</head>
<body>
        <header>
            <?php include "/navbar/navbar.php"; ?>
        </header>
        <div class="container">
        <section class="main row">
        <article class="col-sm-12 col-md-6">
<div id="jugar" class="container"><button id="start">
  ¡Comenzar el juego!
</button></div>



<script> 
document.querySelector("#start").addEventListener('click', function(){
  memoria({
    id: "#jugar",
    col: 4,
    row: 4
  });
  
  this.remove();
})
function memoria(conf){
    if(conf.col % 2 > 0) {
        console.log('Only pair numbers');
        return;
    }
    
    if(conf.row % 2 > 0) {
        console.log('Only pair numbers');
        return;
    }
    
    var d = document, // documento
        c = d.querySelector(conf.id), // contenedor
        t = d.createElement("table"), // tabla
        n = 0, // numeros a mostrar
        p = { // Datos del juego
            points: 0,
            maxPoints: 1000 * (conf.col*conf.row/2),
            perSuccess: 1000,
            clicked: [],
            timeForPlay: Math.ceil((conf.col*conf.row)*3),
            gameOver: false,
            started: false
        },
        data = []; // arreglo de numeros para el juego ordenados aleatoriamente
    
    // numeros con los que se va a jugar
    for(var i = 1; i <= (conf.col*conf.row) / 2; i++){
        data.push(i);
    }
    
    // revolver los numeros
    var data_a = shuffle(data);
    var data_b = shuffle(data);
    
    data = data_a.concat(data_b);
        
    for(var i = 0; i < conf.row; i++){
        var tr = d.createElement("tr");
        
        for(var j = 0; j < conf.col; j++){
            var td = d.createElement("td"),
                span = d.createElement("span");
            
            // numero html interior
            span.innerHTML = data[n];
            
            // añadir evento
            td.addEventListener('click', tdClick);
            
            // adjuntar a tabla
            tr.appendChild(td);
            td.appendChild(span);
            
            n++;
        }
        
        t.appendChild(tr);
    }
    
    // adjuntar al dashboard
    var dashboard = d.createElement("div");
    dashboard.className = 'game-dashboard';
    c.appendChild(dashboard);
    
    // adjuntar a la tabla contenedora
    t.className = 'game-container';
    c.appendChild(t);
    
    
    // adjuntar comentarios al juego
    var comments = d.createElement("div");
    comments.className = 'game-comments';
    c.appendChild(comments);
    
    // empezar juego luego de 5 segundos
    start();
    
    function start(){
        var n = Math.ceil((conf.col * conf.row)*0.3);
        
        var interval = setInterval(function(){
            dashboard.innerHTML = 'El juego empezara en ' + n + ' segundos..';
            
            if(n === 0){
                cleansTds();
                timeElapsed();
                p.started = true;
                
                clearInterval(interval);
            } else n--;
        }, 1000);
    }
    
    function timeElapsed(){
        var interval = setInterval(function(){
            if(p.timeForPlay === 0 || p.maxPoints === p.points) {
                clearInterval(interval);
                gameOver();
            } else {
                dashboard.innerHTML = 'Tienes ' + (p.timeForPlay--) + ' segundos para ganar :) ..';                
            }
        }, 1000);
    }
    
    function gameOver(){
        p.gameOver = true;
        if(p.maxPoints === p.points){ 
            pfinal = 8000*(p.timeForPlay+1); //se multiplica el tiempo por el puntake para obtener un resultado por tiempo
            dashboard.innerHTML = 'Has ganado el juego con ' + pfinal + ' puntos :)';

            if(c===1) //sentencia para que el puntaje del juego se  guarde en la base de datos solo una vez
            {

            $(document).ready(function() {

            var variablejs=pfinal;


            $('#contenedor').load("datosjuego.php",{puntos:variablejs}) //codigo para incluir el php en la pagina

            });


            }
            c=1;          

        }else{
            dashboard.innerHTML = 'Has perdido el juego ..';
        }
    }
    
    function cleansTds(){
        var spans = d.querySelectorAll(conf.id + " span");
        for(var i = 0; i < spans.length; i++){
            if(spans[i].className !== ("success")){
                spans[i].className = 'none';                
            }
        }
    }
    
    function tdClick(){
        var n = this.innerHTML;
        var span = this.childNodes[0];

        if(span.className === 'success' || span.className === 'select' || p.gameOver || !p.started) return;
        
        if(p.clicked.length === 0){
            p.clicked[0] = span;
            span.className = 'select';
        } else if(p.clicked.length === 1){
            p.clicked[1] = span;
            span.className = 'select';
            
            // Correctly
            if(p.clicked[0].innerHTML === p.clicked[1].innerHTML){
                p.clicked[0].className = 'success';
                p.clicked[1].className = 'success';
                p.points += p.perSuccess;
                
                comments.innerHTML = 'Acertaste!! Vamos por mas';
            } else{ // Fails
                p.started = false;
                setTimeout(function(){
                    p.started = true;
                    cleansTds();
                }, 1000);
                comments.innerHTML = 'Fallaste ...';
            }
        }
        
        if(p.clicked.length === 2) {
            p.clicked = [];
        }
        
        if(p.points === p.maxPoints){
            gameOver();
        }
    }
    
    function shuffle(data) {
        var array_new = [],
            array_indexs = [];
        
        for(var i = 0; i < data.length; i++) 
            array_indexs.push(i);

        while(array_indexs.length > 0){
            var index_for_array_indexs = Math.floor((Math.random() * (array_indexs.length)) + 0),
                index = array_indexs[index_for_array_indexs];
            
            array_indexs.splice(index_for_array_indexs, 1);
            array_new.push(data[index]);
        }
        
        return array_new;
    }
}
document.getElementById("myForm").reset();
</script>
<p id="contenedor"></p>
</article>
<aside class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="container">
    <h2>Top 10 Puntajes</h2>
    <?php include"ranking.php" ?>
    </div>
</aside>
</section>
</div>
</body>
</html>
