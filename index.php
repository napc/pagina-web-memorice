<?php
	session_start();/* Abre una sesion preexistente */
	if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {}
	else {
		header('Location: home.php');
		exit;
	}
		/* Sentencia para que solo los usuarios registrados puedan ver el contenido de la pagina*/
	include "/conf/permissions.php";
	if ($perm > 0) {}
	else {
		echo "Tu cuenta ha sido deshabilitada, por favor contacta a un administrador. <br>"."Redireccionando...";
		header('refresh:2; url=/whoweare.php');
		session_destroy();
		echo '<img src="/images/loading.gif" height="70px" width="70px"></img>';
		exit;
	}

	/* Esta sentencia hace que los usuarios deshabilitados no puedan entrar a esta pagina */
		if ($perm > 1) {}
	else {
		echo "No tienes permisos para estar en esta pagina."."Redireccionando...";
		header('refresh:2; url=/game.php');
		echo '<img src="/images/loading.gif" height="70px" width="70px"></img>';
		exit;
	}
	/* Esta sentencia hace que los usuarios anonimos no puedan entrar a la pagina */
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>
		Inicio - M3M0R1C3
		</title>
		<link href="/css/estilos.css" rel="stylesheet" type="text/css"/>
		<meta content="width=device-width, initial-scale=1" name="viewport">
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js">
		</script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js">
		</script>
		<!-- Links para implementar bootstrap y jquery -->
	</link>
	</meta>
</head>
<body>
	<header>
		<!-- Cabecera del sitio, donde va la barra de navegación -->
		<nav>
			<?php include "navbar/navbar.php"; ?>
			<!-- Barra de navegación -->
		</nav>
	</header>
	<div class="container">
		<!-- Este div hace que los elementos queden centrados en la pagina -->
		<section class="main row">
			<!-- Agrupa elementos y los separa en columnas y filas -->
			<article class=" col-md-6"><!-- El articulo ocupa las columnas señaladas en la clase -->
				<h1>
				Hola, bienvenido a M3M0R1C3
				</h1>
				<p>
					M3M0R1C3 es un juego gratis en el que debes memorizar cartas para luego elegir el par de cartas correctos
				</p>
				<a href="game.php">
					<button class="btn btn-primary">
					Jugar
					</button>
				</a>


			</article>
		</section>
	</div>
</body>
</html>