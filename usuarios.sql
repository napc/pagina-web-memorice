-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 28-09-2017 a las 04:33:53
-- Versión del servidor: 5.7.14
-- Versión de PHP: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `usuarios`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `login`
--

CREATE TABLE `login` (
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `ap` varchar(30) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `email` varchar(60) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(1000) NOT NULL,
  `profpic` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL DEFAULT 'noprof.jpg',
  `estado` int(1) NOT NULL,
  `forgotpassid` varchar(30) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `login`
--

INSERT INTO `login` (`name`, `ap`, `email`, `username`, `password`, `profpic`, `estado`, `forgotpassid`) VALUES
('hola', 'hola', 'hola@hola.com', 'hola', '$2y$10$qBiyCu8ONQZj82djSx.cIeXLw/cJ1QdWSTUQjqZ2ktZb5RBb0i7wy', 'noprof.jpg', 3, ''),
('Nicolas', 'Peña Camus', 'nico.apc2@gmail.com', 'n4pc', '$2y$10$yFfD4/NcdhIz2A2mNNcJnerRzbPI3v3OVcnIAgSdMh1ZfKoYaerkm', 'IMG-20170125-WA0009.jpg', 3, ''),
('default', 'user', 'admin@localhost', 'admin', '$2y$10$BnSwHa6A6mMSF5bdTtUIjOJR8OyFIxMP1ctDIcr5kjHTPhl2ACPpy', 'noprof.jpg', 3, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sugerencias`
--

CREATE TABLE `sugerencias` (
  `nombre` varchar(50) NOT NULL DEFAULT 'Anonimo',
  `sugerencia` varchar(500) NOT NULL,
  `fecha_sug` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `sugerencias`
--

INSERT INTO `sugerencias` (`nombre`, `sugerencia`, `fecha_sug`) VALUES
('', 'ghggh', '2017-09-27 00:00:00'),
('hggh', 'ghggh', '2017-09-27 00:00:00'),
('hggh', 'ghggh', '2017-09-27 00:00:00'),
('hggh', 'ghggh', '2017-09-27 19:24:37'),
('sdasd12d', 'sd1212a', '2017-09-27 19:26:38');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`email`,`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `username` (`username`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
